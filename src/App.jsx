import './App.css'
import Footer from "./components/Footer/Footer.jsx";
import Buttoon from "./components/Main/Button/Button.jsx";
import {useState} from "react";
import CustomModal from "./components/Main/Modal/Modal.jsx";





function App() {
    // Хук віддображення модалки з картинкою
    const [imageModal, setImageModal] = useState(false);
    // Хук віддображення кнопок виклику модалки
    const [showBtns, setShowBtns] = useState(true);
    const [simpleModal, setSimpleModal] = useState(false);
    // зміна видимості модалки
    const toggleSimpleModal = () => {
        setSimpleModal(!simpleModal);
    }

    //Зміна видимості модалки
    const handleImageModal = () => setImageModal(true);
    const handleModalClose = () => setImageModal(false);
    //
    const handleBtnsShow = () => {
        setShowBtns(false);
        handleModalClose();
    }

    return (
    <>
        <div className="wrapper">
            {
                showBtns &&
                <>
                    <Buttoon classNames="secondary" click={handleImageModal}>Open first modal</Buttoon>
                    <Buttoon classNames="outline-secondary" click={toggleSimpleModal}>Open second modal</Buttoon>
                </>
            }
        </div>
        {
            imageModal &&
            <CustomModal
                firstText={"Product Delete"}
                secondaryText={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, dolorem?"}
                onHide={handleModalClose}
                handleBtnsShow={handleBtnsShow}
                classNames="modal-image"
                image = {"../src/components/Main/Img/nxoh.jpg"}
                buttons={[
                    {
                        text: "NO, CANCEL",
                        classNames: "secondary",
                        click: handleModalClose,
                    },
                    {
                        text: "YES, DELETE",
                        classNames: "outline-secondary on-modal",
                        click: handleBtnsShow
                    },
                ]}
            />
        }
        {
            simpleModal &&
            <CustomModal
                headerText={"Lorem ipsum dolor sit amet, consectetur adipisicing."}
                firstText={"Add Product 'Name'"}
                secondaryText="Lorem ipsum dolor sit."
                buttons={[
                    {
                        text: "Add to Favorite",
                        classNames: "secondary",
                        click: toggleSimpleModal,
                    }
                ]}
            />
        }
      <Footer />
    </>
  )
}



export default App
