import cn from "classnames";
import "./Button.scss";
import PropTypes from "prop-types";
// import button from "bootstrap/js/src/button.js";
function Buttoon (props) {
    const {type, children, classNames= "", click = () => {}} = props;

    return (
        <button type={type} onClick={click} className={cn("btn", classNames)} >{children}</button>
    )
}

Buttoon.defaultProps = {
    type: "button",
    click: () => {}
}

Buttoon.propTypes = {
    type: PropTypes.string,
    classNames: PropTypes.string,
    click: PropTypes.func,
    children: PropTypes.any
}
export default Buttoon