import PropTypes from "prop-types";

function ModalFrame({children}) {
    return (
        <div className="modal-frame">{children}</div>
    )
}

ModalFrame.propTypes = {
    children: PropTypes.any,
    onHide: PropTypes.func,
}
export default ModalFrame