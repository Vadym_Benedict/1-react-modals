// import Backdrop from "bootstrap/js/src/util/backdrop.js";
import Backdrop from "./Backdrop.jsx"
import {ModalFooter} from "react-bootstrap";
import PropTypes from "prop-types";
import ModalFrame from "./ModalFrame.jsx";
// import {ModalHeader} from "react-bootstrap";
import ModalHead from "./ModalHead.jsx";
import ModalBody from "./ModalBody.jsx";
import Buttoon from "../Button/Button.jsx";
import cn from "classnames";

function CustomModal({children, ...props}) {
    const {
        firstText,
        classNames = "",
        secondaryText,
        headerText,
        onHide,
        image,
        buttons
    } = props;

    return (
        <Backdrop onHide={onHide}>
            <ModalFrame>
                {children}
                <div onClick={onHide} className="modal-close"></div>
                {headerText && <ModalHead headerText={headerText}></ModalHead>}
                <ModalBody firstText={firstText} secondaryText={secondaryText}>
                    {image && (
                        <figure>
                            <img src={image} className={cn(classNames)} alt="modal image" />
                        </figure>
                    )}
                </ModalBody>
                <ModalFooter>
                    {buttons.map((button, index) => (
                        <Buttoon key={index} classNames={button.classNames} click={button.click}>
                            {button.text}
                        </Buttoon>
                    ))}
                </ModalFooter>
            </ModalFrame>
        </Backdrop>
    );
}
CustomModal.propTypes = {
    children: PropTypes.any,
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    headerText: PropTypes.string,
    onHide: PropTypes.func,
    handleBtnsShow: PropTypes.func,
    image: PropTypes.any,
    classNames: PropTypes.string,
    buttons: PropTypes.any
}

CustomModal.defaultProps = {
    headerText: null,
}

export default CustomModal