import "./Modal.scss";
import PropTypes from "prop-types";
function Backdrop({children}) {

    return (
        <div className="backdrop">{children}</div>
    )
}

Backdrop.propTypes = {
    children: PropTypes.any,
}
export default Backdrop
