import PropTypes from "prop-types";

function ModalHead({headerText}) {

    return (
        <>
            <div id="modal-header">{headerText}</div>
            <hr/>
        </>
    )
}

ModalHead.propTypes = {
    headerText: PropTypes.string,
}

export default ModalHead