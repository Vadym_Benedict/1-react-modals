import PropTypes from "prop-types";

function ModalBody({children, firstText, secondaryText}) {

    return (
        <div className="modal-body">
            {children}
            <h2>{firstText}</h2>
            <h5>{secondaryText}</h5>
        </div>
    )
}

ModalBody.propTypes = {
    children: PropTypes.any,
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
}

export default ModalBody